import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';

import AddProduct from './pages/AddProduct'
import AllUsers from './pages/AllUsers';
import Admin from './pages/Admin'
import EditProduct from './pages/EditProduct';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import ShowOrders from './pages/ShowOrders';
import Products from './pages/Products';
import ProductView from './pages/ProductView';

import './App.css';

import { UserProvider } from './UserContext'


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }


  useEffect(() => {
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      if(typeof data._id === "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])

  console.log(user)

  return (

      <UserProvider value={{user, setUser, unsetUser}}>
       <Router>
            <AppNavbar/>
            <Container fluid>
              <Routes>
                  <Route path="/" element={<Home/>} />
                  <Route path="/products" element={<Products/>} />
                  <Route path="/allusers" element={<AllUsers/>} />
                  <Route path="/products/:productId" element={<ProductView/>} />
                  <Route path="/editProduct/:productId" element={<EditProduct/>} />
                  <Route path="/products/addProduct" element={<AddProduct/>} />
                  <Route path="/showOrders" element={<ShowOrders/>} />
                  <Route path="/admin" element={<Admin/>} />
                  <Route path="/login" element={<Login/>} />
                  <Route path="/logout" element={<Logout/>} />
                  <Route path="/register" element={<Register/>} />
              </Routes>
              <Footer />
            </Container>
            
        </Router>
      </UserProvider>  
     
  );
}

export default App;
