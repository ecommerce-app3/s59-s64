import { useState, useEffect, useContext } from 'react';
import { Container, Card, Row, Col, Button, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [qty, setQty] = useState(1);
	const [prodId, setProdId] = useState("")
	const [img, setImg] = useState("");


	function addCart(e) {
			e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/orders`, {
			method: "POST",
			headers: {
				'Content-Type': "application/json",
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: qty
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Product Added to Cart",
					icon: "success",
					text: "Added to Cart Successfully"
				})

				navigate('/products');
			} else {
				Swal.fire({
					title: "Product Order Failed",
					icon: "error",
					text: "Problem encountered during adding product to cart"
				})
			}
		})
	};

	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setImg(data.img);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
		})
	}, [productId])

	return(

		<Row className="my-5">
	
		<Col className="product-view mb-5" lg={6}>

		<Card className="productListCard">
      <Card.Img className="productImg" src={img} style={{ width:'80%' }}/>
    </Card>

		</Col>

		<Col className="product-view mb-5" lg={6} >

		<Card className="productListCard" style={{ border: '0px'}}>
      <Card.Body>
        <Card.Title><strong>{name}</strong></Card.Title>
        <Card.Text style={{ color: 'orange' }}>&#8369;{price}</Card.Text>
        <Card.Text>{description}</Card.Text>
        <Form onSubmit={(e) => addCart(e)}>
        <Form.Group as={Row} className="mb-3 my-3">
        <Row>
       <Form.Label column sm={2}>
          Qty:
        </Form.Label>
      <Form.Control
      	className="formHorizontal"
        type="number"
        value={qty}
        onChange={(e) => {setQty(e.target.value)}}
        sm={12}
        column
      />
      </Row>
      </Form.Group>
        
        {
        	(user.id !== null) ?
        	<Button className="buyNow-btn" type="submit">BUY NOW</Button>
        	:
        	<Button className="buyNow-btn" as={Link} to={`/login`}>Login to Buy</Button>
        }
        </Form>
      </Card.Body>
    </Card>

		</Col>
		</Row>
	)

}