import { useState, useEffect, useContext } from 'react';
import { Row, Col, Table, Button } from 'react-bootstrap';
import { Link, Navigate, useParams } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ShowOrders() {

	const { user } = useContext(UserContext);

	const { orderId } = useParams();

	const [allOrders, setAllOrders] = useState([]);

	const fetchData = () => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setAllOrders(data.map(order => {

				return (


					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{order.userId}</td>
						<td>{order.products[0].productId}</td>
						<td>{order.products[0].quantity}</td>
						<td>{order.totalAmount}</td>
						<td>{order.purchasedOn}</td>
						<td>{order.status == "pending" ? "Pending" : "Shipped" }</td>
						<td>
						{
							(order.status == "pending")
							?
							<> 
							<Button className="mx-1" variant="success" size="sm" onClick ={() => ship(order._id)}>Ship</Button>
							<Button variant="danger" size="sm" onClick ={() => deleteOrder(order._id)}>Delete</Button>
							</>
							:
							<>
							<Button className="mx-1" variant="secondary" size="sm" disabled>Shipped</Button>
							<Button variant="danger" size="sm" onClick ={() => deleteOrder(order._id)}>Delete</Button>
							</>
						}
						</td>
					</tr>
				)
			}))
		})

	}

	const ship = (orderId) => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/status`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				status: "shipped"
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {
				Swal.fire({
					title: "Product Status Updated",
					icon: "success",
					text: "Item shipped"
				})
				fetchData();
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}


	const deleteOrder = (orderId) => {

		fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/delete`, {
			method: "DELETE",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				orderId: orderId
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {
				Swal.fire({
					title: "Order Deleted",
					icon: "success",
					text: "Order has been deleted Successfully"
				})
				fetchData();
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	useEffect(() => {
		fetchData();
	}, [])
		

	return(

		(user.isAdmin) ?
		<>
			<div className="my-5">
				<h1>All Orders</h1>
			</div>
			<Table striped hover>
				<thead>
				<tr>
					<th>Order Id</th>
					<th>User Id</th>
					<th>Product Id</th>
					<th>Qty</th>
					<th>Total Amount</th>
					<th>Date Purchase</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
				</thead>
				<tbody>
					{ allOrders }
				</tbody>
			</Table>
		</>
		:
		<Navigate to="/products" />
	)

}