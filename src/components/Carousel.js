import { useState, useEffect } from 'react';
import { Row, Col, Button, Card, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import c1 from '../images/c1.jpg';
import c2 from '../images/c2.jpg';
import c3 from '../images/landing.jpg';


export default function BestSeller() {

	return (

		<Carousel>
      <Carousel.Item interval={2000}>
        <img
          className="d-block w-100"
          src={c2}
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={2000}>
        <img
          className="d-block w-100"
          src={c1}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item interval={2000}>
        <img
          className="d-block w-100"
          src={c3}
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>

	)
}