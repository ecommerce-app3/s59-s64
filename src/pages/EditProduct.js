import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';

export default function EditProduct() {

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	const [img, setImg] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stock, setStock] = useState(0);

	function editProduct(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				img: img,
				name: name,
				description: description,
				price: price,
				stock: stock
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data == true){
				Swal.fire({
					title: 'Product Updated Successfully',
					icon: 'success',
					text: `${name} is now updated`
				})

				setImg('');
				setName('');
				setDescription('');
				setPrice(0);
				setStock(0);

				navigate('/admin');
			} else {
				Swal.fire({
					title: 'Error Encountered',
					icon: 'error',
					text: 'Something went wrong. Please try again'
				})
			}
		})
	}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setImg(data.img)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setStock(data.stock)
		})
	}, [productId]);

	return(

		user.isAdmin ?
		<>
			<Container className="registration">
			<Row>
			<h1 className="text-register">Edit Product</h1>
				<div className="editProduct-img">
					<img src={img} alt="" style={{width:'12rem'}}/>
				</div>
				<Col className="register-col" xs={12} lg={8}>
				<Form onSubmit={(e) => editProduct(e)}>
						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={img}
									onChange={(e) => {setImg(e.target.value)}}
									placeholder="Product Image"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={name}
									onChange={(e) => {setName(e.target.value)}}
									placeholder="Product Name"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									as="textarea"
									rows="3"
									value={description}
									onChange={(e) => {setDescription(e.target.value)}}
									placeholder="Product Description"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={price}
									onChange={(e) => {setPrice(e.target.value)}}
									placeholder="Price"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={stock}
									onChange={(e) => {setStock(e.target.value)}}
									placeholder="Stock"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="button" lg={6}>
							<Button className="submit-btn" type="submit">Update</Button>
							<Button className="submit-btn" as={Link} to="/admin" type="submit">Cancel</Button>
						</Form.Group>
						
					</Form>
				</Col>
			</Row>
		</Container>
		</>
		:
		<Navigate to="/products" />
	)
}