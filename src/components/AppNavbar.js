import { useContext } from 'react';
import { Navbar, Nav, Container, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import logo1 from '../images/logo1.png'

export default function AppNavbar() {

	const { user } = useContext(UserContext);


	return (

		<Navbar bg="dark" expand="lg" >
		    <Container fluid className="mx-5">
		      <Navbar.Brand as={Link} to="/">
		      	<div>
		      		<img src={logo1} alt="Logo" style={{width: '12rem'}}/>
		      	</div>
		      </Navbar.Brand>
		      <Navbar.Toggle aria-controls="navbarScroll" style={{background:'white'}}/>
		      <Navbar.Collapse id="navbarScroll">
		        <Nav className="ms-auto">
		        	<Nav.Link as={Link} to="/">Home</Nav.Link>
		        	{
		        		(user.isAdmin) ? 
		        		<Nav.Link as={Link} to="/admin">Admin Dashboard</Nav.Link>
		        		:
		        		<Nav.Link as={Link} to="/products">Products</Nav.Link>
		        	}
		        	{
		        		(user.id !== null) ?
		        		<Nav.Link as={Link} to="/Logout">Logout</Nav.Link>
		        		:
		        		<>
		        		<Nav.Link as={Link} to="/login" >Login</Nav.Link>
						<Nav.Link as={Link} to="/register" >Register</Nav.Link>
		          		</>

		        	}
		          	
		          
		        </Nav>
		      </Navbar.Collapse>
		    </Container>
		  </Navbar>
	)
};