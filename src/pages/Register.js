import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [phoneNo, setPhoneNo] = useState("");
	const [pword1, setPword1] = useState("");
	const [pword2, setPword2] = useState("");

	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail/`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true){

				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Already had an account? Try Logging In"
				})
			} else {
				fetch(`http://localhost:4000/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						phoneNo: phoneNo,
						email: email,
						password: pword1
					})
				})
				.then(res => res.json())
				.then(data => {

					if(data === true) {

						setFirstName("");
						setLastName("");
						setPhoneNo("");
						setEmail("");
						setPword1("");
						setPword2("");

						Swal.fire({
							title: "Registered Successfully",
							icon: "success",
							text: "Welcome to Nature Shop!"
						})

						navigate("/login");
					} else {

						Swal.fire({
							title: "Something Went Wrong",
							icon: "error",
							text: "Please try again"
						})	
					}
				})
			}
		})
		
	}

	useEffect(() => {
		if(pword1 === pword2){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [pword1, pword2])

	return (

		(user.id !== null) ?
		<Navigate to="/products" />

		:

		<Container className="registration">
			<Row>
				<h1 className="text-register">Register</h1>
				<Form onSubmit={(e) => registerUser(e)}>
				<Col className="register-col" xs={12} lg={8}>
						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={firstName}
									onChange={(e) => {setFirstName(e.target.value)}}
									placeholder="First Name"
									required
								/>
							</Form.Label>
						</Form.Group>
				</Col>
				<Col className="register-col" xs={12} lg={8}>
						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={lastName}
									onChange={(e) => {setLastName(e.target.value)}}
									placeholder="Last Name"
									required
								/>
							</Form.Label>
						</Form.Group>
				</Col>
				<Col className="register-col" xs={12} lg={8}>
						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={email}
									onChange={(e) => {setEmail(e.target.value)}}
									placeholder="Email"
									required
								/>
							</Form.Label>
						</Form.Group>
				</Col>
				<Col className="register-col" xs={12} lg={8}>
						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={phoneNo}
									onChange={(e) => {setPhoneNo(e.target.value)}}
									placeholder="09xxxxxxxxx"
									required
								/>
							</Form.Label>
						</Form.Group>
				</Col>
				<Col className="register-col" xs={12} lg={8}>
						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="password"
									value={pword1}
									onChange={(e) => {setPword1(e.target.value)}}
									placeholder="Password"
									required
								/>
							</Form.Label>
						</Form.Group>
				</Col>
				<Col className="register-col" xs={12} lg={8}>
						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="password"
									value={pword2}
									onChange={(e) => {setPword2(e.target.value)}}
									placeholder="Confirm Password"
									required
								/>
							</Form.Label>
						</Form.Group>
				</Col>
						<Form.Group className="button" lg={6}>
						{ isActive ?
							<Button className="submit-btn" type="submit">Register</Button>
							:
							<Button className="submit-btn" type="submit" disabled>Register</Button>
						}
						</Form.Group>
						
					</Form>
			</Row>
		</Container>

	)

}