import { useContext, useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Admin() {

	const { user } = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`,{
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllProducts(data.map(product => {

				return (

					<tr key={product._id}>
						<td className="prodImage"><img src={product.img} alt="" style={{width:'30%'}} /></td>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.stock}</td>
						<td>
						{
							(product.isActive)
							?
							<Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
							:
							<>
								<Button variant="success" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
								<Button as={ Link } to={`/editProduct/${product._id}`} variant="secondary" size="sm" className="m-2" >Edit</Button>
							</>
						}
						</td>
					</tr>
				)
			}))
		})
	}

	const archive = (productId, productName) =>{
		
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{

			console.log(data)
			
			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const unarchive = (productId, productName) =>{
		

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	
	useEffect(()=>{
		
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1 className="my-3">Admin Dashboard</h1>
				<Button as={Link} to="/products/addProduct" variant="primary" size="lg" className="mx-2">Add Product</Button>
				<Button as={Link} to="/showOrders" variant="success" size="lg" className="mx-2">Show Orders</Button>
				<Button as={Link} to="/AllUsers" variant="secondary" size="lg" className="mx-2 my-5">Show All Users</Button>
			</div>
			<Table striped hover>
		     <thead>
		       <tr>
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Stocks</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allProducts }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}