import { useState, useEffect } from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import '../App.css';

export default function ProductCard({productProp}) {

	const {img, name, price, _id} = productProp;

	const [addToCart, setAddToCart] = useState(0);

	function cartAdded() {
		if(addToCart > 0) {
			setAddToCart( addToCart + 1);

		}
	}

	return (

		<Col className="productList mb-5 ">

		<Card className="productListCard" style={{ width: '18rem', border: '0px'}}>
      <Card.Img variant="top" src={img} />
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Text style={{ color: 'orange' }}>&#8369;{price}</Card.Text>
        <Button className="buyNow-btn" as={Link} to={`/products/${_id}`}>View Product</Button>
      </Card.Body>
    </Card>

		</Col>

	)

}