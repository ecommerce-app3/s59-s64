import { Fragment } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import Carousel from '../components/Carousel';
import BestSeller from '../components/BestSeller';


export default function Home() {	

	return (
		
		<Container fluid>
			<Row style={{width:'100%'}}>
				<Col className="col-12 col-md-6">
					<div>
						<Carousel />
					</div>
				</Col>
				<Col className="fullwidth-text-col my-5 col-12 col-md-6">
					<div className="fullwidth-text-div">
						<h1 className="fullwidth-text"><strong>Beauty in Technology</strong></h1>
						<h3 className="fullwidth-text">Exquite Inside and Out</h3>
						<p className="fullwidth-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				<div className="fullwidth-text my-5">
					<Button className="buyNow-btn">VIEW DETAILS</Button>
				</div>
				
				</Col>
			</Row>
			<Row>
				<BestSeller/>
			</Row>
		</Container>

			


	)
};