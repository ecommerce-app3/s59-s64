import { Row, Col, Button, Stack } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { MDBIcon } from 'mdb-react-ui-kit';

export default function Footer() {

	return (

		<Row className="footer">
		<Col className="mt-5" lg={6}>
			<div>
				<h6 className="mb-3"><strong>ABOUT US</strong></h6>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				<i className="bi-envelope" style={{color: 'white'}} ></i>
				<a className="mx-2" href="#" style={{color:'white'}}>celestial@gmail.com</a>
			</div>
		</Col>
		<Col className="mt-5" lg={2}>
			<div className="follow-us">
				<h6 className="mb-3"><strong>FOLLOW US</strong></h6>
				<Stack gap={2}>
					<div>
					<i className="bi-facebook mx-2" style={{color: 'white'}} ></i>
					<a href="#" style={{color:'white'}}>Facebook</a><br/>
					</div>

					<div>
					<i className="bi-instagram mx-2" style={{color: 'white'}} ></i>
					<a href="#" style={{color:'white'}}>Instagram</a><br/>
					</div>

					<div>
					<i className="bi-youtube mx-2" style={{color: 'white'}} ></i>
					<a href="#" style={{color:'white'}}>Youtube</a><br/>
					</div>

					<div>
					<i className="bi-tiktok mx-2" style={{color: 'white'}} ></i>
					<a href="#" style={{color:'white'}}>Tiktok</a><br/>
					</div>
				</Stack>
			</div>
		</Col>
		<Col className="mt-5" lg={2}>
			<div className="cust-service">
				<h6 className="mb-3"><strong>CUSTOMER SERVICE</strong></h6>
				<Stack gap={2}>

					<div><a className="mx-2" href="#" style={{color:'white'}}>Shipping</a><br/></div>

					<div><a className="mx-2" href="#" style={{color:'white'}}>Return Policy</a><br/></div>

					<div><a className="mx-2" href="#" style={{color:'white'}}>Terms & Conditions</a><br/></div>

					<div><a className="mx-2" href="#" style={{color:'white'}}>Privacy Policy</a><br/></div>
				</Stack>
			</div>
		</Col>
		<Col className="mt-5" lg={2}>
			<div className="about-brand">
				<h6 className="mb-3"><strong>ABOUT BRAND</strong></h6>
				<Stack gap={2}>
				<div><a className="mx-2" href="#" style={{color:'white'}}>Our Story</a><br/></div>

				<div><a className="mx-2" href="#" style={{color:'white'}}>Beauty Tech</a><br/></div>

				<div><a className="mx-2" href="#" style={{color:'white'}}>Quality Promise</a><br/></div>

				<div><a className="mx-2" href="#" style={{color:'white'}}>Store Location</a><br/></div>

				<div><a className="mx-2" href="#" style={{color:'white'}}>Contact Us</a><br/></div>
				</Stack>
			</div>
		</Col>
	</Row>

	)
}