import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(){

	const navigate = useNavigate();

	const { user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState("");
	const [pword1, setPword1] = useState("");

	
	function loginUser(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: pword1
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token',data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Successful Login",
					icon: "success",
					text: "Logged in Successfully"
				})

				setEmail("");
				setPword1("");

				
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Incorrect Email or Password"
				})
			}
		})
	}

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUser({
				id: data._id,
				isAdmin:data.isAdmin
			})
		})
	};


	return (

		(user.id !== null) ?
		<Navigate to="/products"/>
		:
		<Container className="registration">
			<Row>
				<h1 className="text-register">Login</h1>
				<Col className="loginForm" xs={12} lg={5}>
					<Form  onSubmit={(e) => loginUser(e)}>
						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={email}
									onChange={(e) => {setEmail(e.target.value)}}
									placeholder="Email"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="password"
									value={pword1}
									onChange={(e) => {setPword1(e.target.value)}}
									placeholder="Password"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="button" lg={6}>
							<Button className="submit-btn" type="submit">Login</Button>
						</Form.Group>
						
					</Form>
				</Col>
			</Row>
		</Container>

	)

}