import { Fragment, useState, useEffect } from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import ProductCard from './ProductCard';

export default function BestSeller() {

	const [bestProducts, setBestProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`,{
		})
		.then(res => res.json())
		.then(data => {
			
			let data2 = [];
			console.log(data)

			data.map(bestProd => {

				if(bestProd.stock < 35) {

					data2.push(bestProd);
					
					
				}

			})

			console.log(data2)
			data2 = data2.slice(0,4)
			console.log(data2)

			setBestProducts(data2.map(bestProduct => {

				return (
						<ProductCard key={bestProduct._id} productProp={bestProduct} />
					)
			}))

		})
	}, [])

	return(

		<Fragment>
		<Row className="my-5">
		<h1><strong>Best Sellers</strong></h1>
		{bestProducts}
		</Row>
		</Fragment>
	)

}