import { Fragment, useState, useEffect } from 'react';
import { Row } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product} />
				)
			}))
		})
	}, [])

	return (
		<Fragment>
				<h1 className="text-center my-5">Products</h1>
				<Row style={{ margin: '20px'}}>
				{products}
				</Row>
			</Fragment>
	)

}