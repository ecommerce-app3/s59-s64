import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function AddProduct() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [img, setImg] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [stock, setStock] = useState("")
	const [price, setPrice] = useState("");


	function addProduct(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				img: img,
				name: name,
				description: description,
				stock, stock,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data === true){

				Swal.fire({
					title: "Product Added Successfully",
					icon: "success",
					text: `${name} is Successfully added to Product List`
				})
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Problem encountered during adding of Product"
				})
			}
		})

		setImg("")
		setName("");
		setDescription("");
		setStock("");
		setPrice("");
	}

	return(

		user.isAdmin ?
		<>
			<Container className="registration">
			<Row>
				<h1 className="text-register">Add Product</h1>
				<Col className="register-col" xs={12} lg={8}>
					<Form onSubmit={(e) => addProduct(e)}>
						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									as="textarea"
									rows="3"
									value={img}
									onChange={(e) => {setImg(e.target.value)}}
									placeholder="Product Image Link"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={name}
									onChange={(e) => {setName(e.target.value)}}
									placeholder="Product Name"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									as="textarea"
									rows="3"
									value={description}
									onChange={(e) => {setDescription(e.target.value)}}
									placeholder="Product Description"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={stock}
									onChange={(e) => {setStock(e.target.value)}}
									placeholder="Stock"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="form-field" lg={6}>
							<Form.Label className="label">
								<Form.Control 
									className="inputs"
									type="text"
									value={price}
									onChange={(e) => {setPrice(e.target.value)}}
									placeholder="Price"
									required
								/>
							</Form.Label>
						</Form.Group>

						<Form.Group className="button" lg={6}>
							<Button className="submit-btn" type="submit">Add Product</Button>
						</Form.Group>
						
					</Form>
				</Col>
			</Row>
		</Container>
		</>
		:
		<Navigate to="/products" />
	)
}

