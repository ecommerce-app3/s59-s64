import { useContext, useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Admin() {

	const { user } = useContext(UserContext);

	const [allUsers, setAllUser] = useState([]);

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/users/all`,{
			headers: {
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)
			setAllUser(data.map(user => {

				return (

					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.firstName} </td>
						<td>{user.lastName}</td>
						<td>{user.phoneNo}</td>
						<td>{user.email}</td>
						<td> {
							(user.isAdmin) ? "Admin" : "Customer"
						} 
						</td>
						<td>
						{
							(user.isAdmin) 
							?
							<Button variant="danger" size="sm" onClick ={() => setCust(user._id, user.firstName)}>Set as User</Button>
							:
							
							<Button variant="success" size="sm" onClick={() => setAdmin(user._id, user.firstName)}>Set as Admin</Button>
								
						}
						</td>
					</tr>
				)
			}))
		})
	}

	const setAdmin = (userId, userName) =>{
		
		fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: userId,
				isAdmin:true
			})
		})
		.then(res => res.json())
		.then(data =>{

			console.log(data)
			
			if(data){
				Swal.fire({
					title: "User's status updated!",
					icon: "success",
					text: `${userName} is now an Admin.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Update Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	const setCust = (userId, userName) =>{
		

		fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: userId,
				isAdmin:false
			})
		})
		.then(res => res.json())
		.then(data =>{

			console.log(data)
			
			if(data){
				Swal.fire({
					title: "User's status updated!",
					icon: "success",
					text: `${userName} is now a Customer.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Update Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	
	useEffect(()=>{
		
		fetchData();
	}, [])

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1 className="my-3">All Users</h1>
			</div>
			<Table striped hover>
		     <thead>
		       <tr>
		         <th>Product ID</th>
		         <th>First Name</th>
		         <th>Last Name</th>
		         <th>Phone No</th>
		         <th>Email</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allUsers }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}